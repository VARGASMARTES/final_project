# final_project
Rosa M. Vargas Martes: final project material, Computational Methods in Atmospheric and Oceanic Sciences - AOS 573

---
REPOSITORY DESCRIPTION 
---

OBJECTIVE: The main objective of this repository is to provide the python code used to carry out: regridding, mapping, and visualization techniques applied to East Pacific Easterly Wave (PEW) linear regressions and the geographic domain in which they occur (150°W-75°W and 10°S–30N°). 

DATA: The data in this repository was pre-processed in MATLAB. The pre-processing was carried out by: (1) taking data from the European Centre for Medium-range Weather Forecasting (ECMWF), Fifth Reanalysis (ERA 5), which can be accessed at the Copernicus Climate Change Service 2017 (https://cds.climate.copernicus.eu/cdsapp#!/search?text=ERA5%20back%20extension&type=dataset), at single and pressure levels, (2) taking precipitation data from the Tropical Rainfall Measuring Mission (TRMM), which can be accessed at the Global Precipitation Measurement (GPM) website (https://gpm.nasa.gov/data/directory). The ERA5 data variables used in the study are mean total precipitation rate (precip), estimated precipitation (Pest) (Ahmed et al. 2020), outgoing long wave radiation (OLR), geopotential height at 700hPa (z), and horizontal winds (u and v) at 700hPa. For TRMM precip only one variable was analyzed: accumulated precipitation (precip_trmm). For the regridding step, precip and precip_trmm were saved for the first 12 hours on 1-July-2016 at a spatial resolution of 2.5°x2.5°. For the mapping and visualization portion of the analysis, the anomalies were determined, and then a Lanczos filter (Duchon 1979) was applied to the data (2-6 days) to retain the variability associated with PEWs. Following this, an Empirical Orthogonal Function (EOF) analysis was applied to the OLR field to retain the relevant pattern in the data. The other variables were then linearly regressed onto the leading orthogonal pair (PC1 and PC2) of the OLR. The data files (.mat format) for this project are included in a separate folder within this repository and are the result of the described statistical analysis techniques. 

---
FILE DESCRIPTION 
---
SUMMARY: The following is a brief description of each of the files and folders included in this repository.

FILES: 

1. AOS573_F21_final_project.yml: This file contains the exported environment used to develop the code included in the jupyter notebook within this repository. This contains the environment associated with the class "AOS573_F21", during the process of exporting the environment the filename was changed to avoid overwriting anything. 

2. VargasMartes_FinalProject.ipynb: This is the jupyter notebook that contains all the code developed for the final project.

3. data_aos573/: Folder that houses all the data used to carry out this final project. As described above, all the data is in .mat format and includes:

    TRMM: lat and lon (lat_trmm.mat and lon_trmm.mat), precip_trmm linear regressions (S_precip_trmm_PC1.mat and S_precip_trmm_PC2.mat), precip_trmm           for the first 12 hours on 1-July-2016 (precip_trmm_12hr_1Jul2016_pac.mat).
    ERA5: lat and lon (lat.mat and lon.mat), Pest linear regressions (Pest_total_Pacific_PC1.mat and Pest_total_Pacific_PC2.mat), precip linear                 regressions (S_precip_Pacific_PC1_ns.mat and S_precip_Pacific_PC2_ns.mat), statistically significant u and v (S_u_PC1_ss.mat,                         S_u_PC2_ss.mat, S_v_PC1_ss.mat, and S_v_PC2_ss.mat), and z (S_z_PC1.mat and S_z_PC2.mat).



REFERENCES: 

Ahmed, F., Á. F. Adames, and J. D. Neelin, 2020: Deep convective adjustment of temperature and moisture. Journal of the Atmospheric Sciences, 77 (6), 2163 – 2186, doi:https://doi.org/10.1175/JAS-D-19-0227.1.

Duchon, C. E., 1979: Lanczos filtering in one and two dimensions. Journal of Applied Meteorology and Climatology, 18 (8), 1016 – 1022, doi:https://doi.org/10.1175/1520-0450(1979)018<1016:LFIOAT>2.0.CO;2. 